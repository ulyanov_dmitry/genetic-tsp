import { injectGlobal } from 'styled-components'

export const injectGlobalStyles = () => {
  injectGlobal`
    html, body {
      -webkit-font-smoothing: antialiased;
      color: #f2f2f2;
      font-family: Roboto, -apple-system, sans-serif;
      margin: 0;
    }
    #root {
      display: flex;
      flex-direction: column;
      width: 100vw;
      height: 100vh;
    }
    * {
      box-sizing: border-box;
    }
  `
}

export const createBoxShadow = () => `
  0 3px 1px -2px rgba(0,0,0,.2),
  0 2px 2px 0 rgba(0,0,0,.14),
  0 1px 5px 0 rgba(0,0,0,.12)
`
