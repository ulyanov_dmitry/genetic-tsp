import 'typeface-roboto'
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import { injectGlobalStyles } from 'styles'
import { makeStore } from 'store'

import { App } from 'components'

injectGlobalStyles()

const store = makeStore()

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
)
