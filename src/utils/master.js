function distance(p1, p2) {
  return euclidean(p1.x - p2.x, p1.y - p2.y)
}

function euclidean(dx, dy) {
  return Math.sqrt(dx * dx + dy * dy)
}

function randomNumber(boundary) {
  return parseInt(Math.random() * boundary, 10)
}

function shuffle(arr) {
  let i = arr.length - 1
  let j
  let x

  while (i) {
    j = randomNumber(i)
    x = arr[--i]
    arr[i] = arr[j]
    arr[j] = x
  }

  return arr
}

function randomIndividual(n) {
  const arr = []

  for (let i = 0; i < n; i++) {
    arr.push(i)
  }

  return shuffle(arr)
}

function calculateDistances(points) {
  const pointsCount = points.length
  const distances = new Array(pointsCount)

  for (let i = 0; i < pointsCount; i++) {
    distances[i] = new Array(pointsCount)
    for (let j = 0; j < pointsCount; j++) {
      distances[i][j] = ~~distance(points[i], points[j])
    }
  }

  return distances
}

const start = input => {
  const { points, populationSize } = input
  const population = []

  try {
    const distances = calculateDistances(points)

    for (let i = 0; i < populationSize; i++) {
      population.push(randomIndividual(points.length))
    }

    console.log(distances)
  } catch (error) {
    console.log(error)
  }
}

export default start
