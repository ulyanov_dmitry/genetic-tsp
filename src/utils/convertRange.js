import { curry } from 'ramda'

const delta = ({ max, min }) => max - min

/***
 * Example:
 * const prevRange = { min: 0, max: 100 }
 * const newRange = { min: 50, max: 120 }
 * const prevValue = 50
 * const newValue = convertRange(prevRange, newRange, prevValue)
 * // newValue === 85
 *
 * @param prevRange
 * @param newRange
 * @param value
 * */
export const convertRange = curry((prevRange, newRange, value) => {
  const prevDelta = delta(prevRange)
  const newDelta = delta(newRange)
  return (value - prevRange.min) * (newDelta / prevDelta) + newRange.min
})

export default convertRange
