import { applyMiddleware, compose, createStore } from 'redux'
import { createLogger } from 'redux-logger'
import rootReducer from './app'

let store

export const dispatch = (...args) => store.dispatch(...args)

export const select = selector => selector(store.getState())

export const makeStore = (initialState = {}) => {
  const middlewares = []

  /* todo Check node env */
  if (1) {
    const logger = createLogger({
      collapsed: true,
      colors: {
        title: () => '#2B8CB9',
        prevState: () => '#455A64',
        action: () => '#3A9EA7',
        nextState: () => '#455A64',
        error: () => '#E5534D',
      },
      timestamp: false,
    })
    middlewares.push(logger)
  }

  store = createStore(
    rootReducer,
    initialState,
    compose(applyMiddleware(...middlewares)),
  )

  return store
}
