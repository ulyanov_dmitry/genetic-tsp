import { createAction, handleActions } from 'redux-actions'

export const updateState = createAction('Update state')

const handlers = {
  [updateState]: (state, { payload }) => ({ ...state, ...payload }),
}

const initialState = {
  best: null,
  running: null,
}

export default handleActions(handlers, initialState)
