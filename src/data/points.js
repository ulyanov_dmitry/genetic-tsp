export const POINTS = {
  byNumber: {
    40: [
      { x: 114, y: 544 },
      { x: 116, y: 404 },
      { x: 120, y: 477 },
      { x: 127, y: 118 },
      { x: 130, y: 196 },
      { x: 147, y: 213 },
      { x: 16, y: 97 },
      { x: 161, y: 617 },
      { x: 163, y: 357 },
      { x: 204, y: 601 },
      { x: 238, y: 313 },
      { x: 245, y: 246 },
      { x: 275, y: 593 },
      { x: 421, y: 377 },
      { x: 425, y: 461 },
      { x: 430, y: 536 },
      { x: 456, y: 154 },
      { x: 534, y: 286 },
      { x: 539, y: 340 },
      { x: 542, y: 313 },
      { x: 550, y: 124 },
      { x: 57, y: 572 },
      { x: 577, y: 545 },
      { x: 601, y: 504 },
      { x: 611, y: 578 },
      { x: 612, y: 644 },
      { x: 620, y: 29 },
      { x: 627, y: 373 },
      { x: 664, y: 190 },
      { x: 700, y: 47 },
      { x: 704, y: 104 },
      { x: 742, y: 170 },
      { x: 794, y: 328 },
      { x: 808, y: 49 },
      { x: 844, y: 294 },
      { x: 847, y: 523 },
      { x: 855, y: 139 },
      { x: 860, y: 466 },
      { x: 863, y: 35 },
      { x: 864, y: 125 },
    ],
    200: [
      { x: 100, y: 254 },
      { x: 121, y: 498 },
      { x: 126, y: 133 },
      { x: 126, y: 86 },
      { x: 129, y: 509 },
      { x: 135, y: 190 },
      { x: 138, y: 191 },
      { x: 139, y: 346 },
      { x: 144, y: 203 },
      { x: 148, y: 604 },
      { x: 150, y: 172 },
      { x: 160, y: 497 },
      { x: 166, y: 565 },
      { x: 171, y: 609 },
      { x: 172, y: 181 },
      { x: 186, y: 284 },
      { x: 187, y: 431 },
      { x: 187, y: 498 },
      { x: 188, y: 542 },
      { x: 198, y: 47 },
      { x: 200, y: 515 },
      { x: 202, y: 293 },
      { x: 202, y: 341 },
      { x: 206, y: 227 },
      { x: 21, y: 401 },
      { x: 225, y: 634 },
      { x: 23, y: 465 },
      { x: 24, y: 312 },
      { x: 243, y: 433 },
      { x: 250, y: 231 },
      { x: 26, y: 270 },
      { x: 263, y: 156 },
      { x: 267, y: 184 },
      { x: 269, y: 135 },
      { x: 279, y: 520 },
      { x: 283, y: 18 },
      { x: 288, y: 406 },
      { x: 289, y: 110 },
      { x: 29, y: 208 },
      { x: 29, y: 412 },
      { x: 292, y: 615 },
      { x: 293, y: 141 },
      { x: 293, y: 220 },
      { x: 3, y: 406 },
      { x: 30, y: 593 },
      { x: 300, y: 224 },
      { x: 300, y: 235 },
      { x: 300, y: 41 },
      { x: 300, y: 439 },
      { x: 31, y: 376 },
      { x: 312, y: 636 },
      { x: 314, y: 325 },
      { x: 314, y: 35 },
      { x: 318, y: 453 },
      { x: 319, y: 412 },
      { x: 319, y: 542 },
      { x: 325, y: 96 },
      { x: 328, y: 370 },
      { x: 328, y: 531 },
      { x: 337, y: 487 },
      { x: 338, y: 211 },
      { x: 342, y: 141 },
      { x: 343, y: 640 },
      { x: 344, y: 112 },
      { x: 344, y: 152 },
      { x: 346, y: 194 },
      { x: 347, y: 423 },
      { x: 350, y: 423 },
      { x: 359, y: 431 },
      { x: 374, y: 38 },
      { x: 375, y: 584 },
      { x: 381, y: 281 },
      { x: 391, y: 571 },
      { x: 4, y: 480 },
      { x: 4, y: 58 },
      { x: 40, y: 236 },
      { x: 400, y: 80 },
      { x: 401, y: 117 },
      { x: 406, y: 221 },
      { x: 411, y: 203 },
      { x: 421, y: 469 },
      { x: 423, y: 592 },
      { x: 426, y: 348 },
      { x: 426, y: 536 },
      { x: 429, y: 427 },
      { x: 439, y: 124 },
      { x: 447, y: 258 },
      { x: 452, y: 600 },
      { x: 453, y: 182 },
      { x: 454, y: 34 },
      { x: 455, y: 215 },
      { x: 457, y: 378 },
      { x: 458, y: 220 },
      { x: 458, y: 479 },
      { x: 462, y: 564 },
      { x: 468, y: 588 },
      { x: 471, y: 212 },
      { x: 471, y: 355 },
      { x: 471, y: 552 },
      { x: 476, y: 281 },
      { x: 476, y: 457 },
      { x: 479, y: 646 },
      { x: 502, y: 582 },
      { x: 506, y: 231 },
      { x: 508, y: 79 },
      { x: 509, y: 257 },
      { x: 511, y: 290 },
      { x: 519, y: 357 },
      { x: 520, y: 339 },
      { x: 523, y: 388 },
      { x: 523, y: 449 },
      { x: 524, y: 486 },
      { x: 528, y: 363 },
      { x: 530, y: 512 },
      { x: 530, y: 648 },
      { x: 534, y: 396 },
      { x: 535, y: 393 },
      { x: 540, y: 212 },
      { x: 541, y: 402 },
      { x: 545, y: 90 },
      { x: 558, y: 288 },
      { x: 560, y: 109 },
      { x: 566, y: 329 },
      { x: 571, y: 355 },
      { x: 572, y: 600 },
      { x: 573, y: 424 },
      { x: 582, y: 275 },
      { x: 59, y: 262 },
      { x: 600, y: 239 },
      { x: 600, y: 441 },
      { x: 601, y: 322 },
      { x: 603, y: 587 },
      { x: 609, y: 576 },
      { x: 614, y: 610 },
      { x: 619, y: 220 },
      { x: 620, y: 545 },
      { x: 622, y: 70 },
      { x: 625, y: 349 },
      { x: 626, y: 216 },
      { x: 629, y: 375 },
      { x: 635, y: 352 },
      { x: 637, y: 57 },
      { x: 643, y: 341 },
      { x: 648, y: 477 },
      { x: 650, y: 266 },
      { x: 656, y: 534 },
      { x: 66, y: 296 },
      { x: 665, y: 336 },
      { x: 668, y: 102 },
      { x: 675, y: 76 },
      { x: 683, y: 291 },
      { x: 685, y: 73 },
      { x: 690, y: 649 },
      { x: 697, y: 414 },
      { x: 701, y: 313 },
      { x: 708, y: 173 },
      { x: 712, y: 592 },
      { x: 717, y: 189 },
      { x: 717, y: 222 },
      { x: 719, y: 642 },
      { x: 723, y: 23 },
      { x: 725, y: 420 },
      { x: 727, y: 290 },
      { x: 729, y: 89 },
      { x: 735, y: 60 },
      { x: 739, y: 91 },
      { x: 74, y: 457 },
      { x: 740, y: 495 },
      { x: 748, y: 334 },
      { x: 761, y: 574 },
      { x: 763, y: 291 },
      { x: 772, y: 72 },
      { x: 775, y: 341 },
      { x: 780, y: 126 },
      { x: 783, y: 520 },
      { x: 788, y: 334 },
      { x: 789, y: 153 },
      { x: 789, y: 163 },
      { x: 79, y: 451 },
      { x: 793, y: 585 },
      { x: 796, y: 598 },
      { x: 797, y: 659 },
      { x: 809, y: 105 },
      { x: 811, y: 355 },
      { x: 811, y: 480 },
      { x: 815, y: 92 },
      { x: 818, y: 252 },
      { x: 822, y: 244 },
      { x: 83, y: 612 },
      { x: 834, y: 339 },
      { x: 845, y: 248 },
      { x: 848, y: 246 },
      { x: 865, y: 515 },
      { x: 865, y: 521 },
      { x: 869, y: 15 },
      { x: 869, y: 337 },
      { x: 87, y: 647 },
      { x: 878, y: 621 },
      { x: 89, y: 635 },
      { x: 9, y: 48 },
    ],
    500: [
      { x: 1, y: 260 },
      { x: 100, y: 572 },
      { x: 102, y: 14 },
      { x: 106, y: 406 },
      { x: 106, y: 487 },
      { x: 109, y: 109 },
      { x: 109, y: 190 },
      { x: 111, y: 240 },
      { x: 111, y: 627 },
      { x: 112, y: 322 },
      { x: 114, y: 650 },
      { x: 116, y: 260 },
      { x: 116, y: 408 },
      { x: 118, y: 337 },
      { x: 12, y: 139 },
      { x: 120, y: 196 },
      { x: 121, y: 421 },
      { x: 121, y: 625 },
      { x: 125, y: 570 },
      { x: 126, y: 317 },
      { x: 127, y: 542 },
      { x: 129, y: 122 },
      { x: 13, y: 500 },
      { x: 132, y: 551 },
      { x: 134, y: 405 },
      { x: 134, y: 63 },
      { x: 137, y: 610 },
      { x: 139, y: 84 },
      { x: 14, y: 370 },
      { x: 140, y: 324 },
      { x: 141, y: 210 },
      { x: 141, y: 640 },
      { x: 143, y: 232 },
      { x: 146, y: 233 },
      { x: 15, y: 34 },
      { x: 150, y: 115 },
      { x: 150, y: 579 },
      { x: 151, y: 451 },
      { x: 153, y: 27 },
      { x: 153, y: 534 },
      { x: 154, y: 371 },
      { x: 154, y: 476 },
      { x: 157, y: 349 },
      { x: 158, y: 53 },
      { x: 159, y: 277 },
      { x: 16, y: 534 },
      { x: 161, y: 340 },
      { x: 165, y: 277 },
      { x: 165, y: 570 },
      { x: 168, y: 342 },
      { x: 168, y: 621 },
      { x: 17, y: 98 },
      { x: 174, y: 154 },
      { x: 175, y: 460 },
      { x: 176, y: 382 },
      { x: 177, y: 156 },
      { x: 178, y: 53 },
      { x: 185, y: 203 },
      { x: 189, y: 432 },
      { x: 191, y: 272 },
      { x: 192, y: 279 },
      { x: 192, y: 535 },
      { x: 198, y: 431 },
      { x: 198, y: 508 },
      { x: 2, y: 142 },
      { x: 2, y: 239 },
      { x: 201, y: 387 },
      { x: 207, y: 123 },
      { x: 207, y: 524 },
      { x: 209, y: 381 },
      { x: 217, y: 459 },
      { x: 218, y: 531 },
      { x: 22, y: 646 },
      { x: 220, y: 280 },
      { x: 221, y: 160 },
      { x: 222, y: 518 },
      { x: 227, y: 167 },
      { x: 227, y: 559 },
      { x: 229, y: 8 },
      { x: 230, y: 340 },
      { x: 231, y: 460 },
      { x: 232, y: 115 },
      { x: 234, y: 501 },
      { x: 235, y: 477 },
      { x: 237, y: 213 },
      { x: 238, y: 485 },
      { x: 24, y: 421 },
      { x: 240, y: 38 },
      { x: 241, y: 469 },
      { x: 241, y: 7 },
      { x: 242, y: 335 },
      { x: 243, y: 236 },
      { x: 255, y: 283 },
      { x: 257, y: 329 },
      { x: 259, y: 599 },
      { x: 263, y: 148 },
      { x: 263, y: 78 },
      { x: 268, y: 472 },
      { x: 27, y: 194 },
      { x: 270, y: 356 },
      { x: 271, y: 208 },
      { x: 272, y: 432 },
      { x: 275, y: 33 },
      { x: 275, y: 610 },
      { x: 276, y: 272 },
      { x: 278, y: 163 },
      { x: 279, y: 395 },
      { x: 281, y: 91 },
      { x: 282, y: 403 },
      { x: 282, y: 527 },
      { x: 283, y: 618 },
      { x: 285, y: 556 },
      { x: 286, y: 629 },
      { x: 287, y: 348 },
      { x: 29, y: 401 },
      { x: 292, y: 118 },
      { x: 292, y: 130 },
      { x: 298, y: 527 },
      { x: 30, y: 648 },
      { x: 302, y: 496 },
      { x: 302, y: 92 },
      { x: 303, y: 197 },
      { x: 306, y: 86 },
      { x: 307, y: 418 },
      { x: 308, y: 175 },
      { x: 311, y: 451 },
      { x: 316, y: 358 },
      { x: 317, y: 251 },
      { x: 317, y: 94 },
      { x: 318, y: 47 },
      { x: 319, y: 404 },
      { x: 319, y: 623 },
      { x: 321, y: 37 },
      { x: 322, y: 118 },
      { x: 322, y: 218 },
      { x: 324, y: 284 },
      { x: 324, y: 516 },
      { x: 328, y: 314 },
      { x: 329, y: 100 },
      { x: 33, y: 147 },
      { x: 331, y: 179 },
      { x: 331, y: 405 },
      { x: 331, y: 68 },
      { x: 334, y: 419 },
      { x: 335, y: 269 },
      { x: 335, y: 270 },
      { x: 335, y: 332 },
      { x: 337, y: 450 },
      { x: 339, y: 85 },
      { x: 34, y: 50 },
      { x: 340, y: 105 },
      { x: 340, y: 493 },
      { x: 340, y: 562 },
      { x: 342, y: 177 },
      { x: 342, y: 253 },
      { x: 344, y: 287 },
      { x: 344, y: 492 },
      { x: 345, y: 290 },
      { x: 345, y: 575 },
      { x: 345, y: 610 },
      { x: 347, y: 196 },
      { x: 351, y: 0 },
      { x: 353, y: 405 },
      { x: 355, y: 167 },
      { x: 359, y: 252 },
      { x: 36, y: 272 },
      { x: 363, y: 406 },
      { x: 365, y: 229 },
      { x: 366, y: 121 },
      { x: 367, y: 476 },
      { x: 371, y: 277 },
      { x: 371, y: 337 },
      { x: 373, y: 189 },
      { x: 373, y: 288 },
      { x: 373, y: 357 },
      { x: 375, y: 355 },
      { x: 375, y: 541 },
      { x: 377, y: 216 },
      { x: 379, y: 38 },
      { x: 38, y: 161 },
      { x: 38, y: 484 },
      { x: 381, y: 168 },
      { x: 381, y: 633 },
      { x: 382, y: 125 },
      { x: 382, y: 539 },
      { x: 383, y: 335 },
      { x: 386, y: 273 },
      { x: 389, y: 451 },
      { x: 39, y: 619 },
      { x: 394, y: 160 },
      { x: 399, y: 579 },
      { x: 4, y: 10 },
      { x: 4, y: 566 },
      { x: 400, y: 381 },
      { x: 405, y: 103 },
      { x: 405, y: 629 },
      { x: 407, y: 65 },
      { x: 408, y: 550 },
      { x: 410, y: 288 },
      { x: 410, y: 401 },
      { x: 410, y: 500 },
      { x: 411, y: 615 },
      { x: 412, y: 189 },
      { x: 412, y: 599 },
      { x: 414, y: 14 },
      { x: 417, y: 656 },
      { x: 417, y: 657 },
      { x: 419, y: 170 },
      { x: 42, y: 239 },
      { x: 42, y: 451 },
      { x: 42, y: 650 },
      { x: 422, y: 73 },
      { x: 425, y: 20 },
      { x: 425, y: 547 },
      { x: 426, y: 212 },
      { x: 428, y: 614 },
      { x: 43, y: 167 },
      { x: 432, y: 305 },
      { x: 432, y: 448 },
      { x: 434, y: 19 },
      { x: 437, y: 419 },
      { x: 437, y: 549 },
      { x: 442, y: 598 },
      { x: 444, y: 184 },
      { x: 452, y: 237 },
      { x: 452, y: 291 },
      { x: 459, y: 195 },
      { x: 459, y: 357 },
      { x: 459, y: 659 },
      { x: 460, y: 358 },
      { x: 463, y: 562 },
      { x: 465, y: 101 },
      { x: 467, y: 234 },
      { x: 477, y: 18 },
      { x: 478, y: 491 },
      { x: 482, y: 176 },
      { x: 482, y: 301 },
      { x: 482, y: 73 },
      { x: 494, y: 575 },
      { x: 500, y: 579 },
      { x: 501, y: 514 },
      { x: 505, y: 126 },
      { x: 506, y: 418 },
      { x: 508, y: 170 },
      { x: 511, y: 263 },
      { x: 513, y: 566 },
      { x: 515, y: 73 },
      { x: 517, y: 351 },
      { x: 522, y: 101 },
      { x: 524, y: 583 },
      { x: 524, y: 604 },
      { x: 527, y: 658 },
      { x: 529, y: 509 },
      { x: 533, y: 468 },
      { x: 533, y: 479 },
      { x: 534, y: 450 },
      { x: 534, y: 561 },
      { x: 535, y: 317 },
      { x: 536, y: 539 },
      { x: 537, y: 174 },
      { x: 539, y: 450 },
      { x: 539, y: 83 },
      { x: 54, y: 411 },
      { x: 541, y: 96 },
      { x: 543, y: 240 },
      { x: 545, y: 292 },
      { x: 546, y: 343 },
      { x: 546, y: 479 },
      { x: 546, y: 522 },
      { x: 548, y: 12 },
      { x: 548, y: 458 },
      { x: 549, y: 154 },
      { x: 55, y: 465 },
      { x: 551, y: 539 },
      { x: 556, y: 112 },
      { x: 56, y: 619 },
      { x: 561, y: 107 },
      { x: 563, y: 71 },
      { x: 57, y: 288 },
      { x: 570, y: 27 },
      { x: 574, y: 362 },
      { x: 575, y: 465 },
      { x: 575, y: 614 },
      { x: 58, y: 268 },
      { x: 581, y: 449 },
      { x: 581, y: 577 },
      { x: 582, y: 5 },
      { x: 582, y: 600 },
      { x: 583, y: 426 },
      { x: 587, y: 374 },
      { x: 588, y: 384 },
      { x: 589, y: 296 },
      { x: 591, y: 26 },
      { x: 592, y: 452 },
      { x: 593, y: 296 },
      { x: 6, y: 185 },
      { x: 600, y: 221 },
      { x: 600, y: 288 },
      { x: 600, y: 610 },
      { x: 601, y: 518 },
      { x: 602, y: 145 },
      { x: 602, y: 592 },
      { x: 603, y: 592 },
      { x: 607, y: 622 },
      { x: 607, y: 98 },
      { x: 610, y: 617 },
      { x: 612, y: 395 },
      { x: 613, y: 645 },
      { x: 617, y: 407 },
      { x: 62, y: 590 },
      { x: 623, y: 118 },
      { x: 626, y: 61 },
      { x: 628, y: 605 },
      { x: 630, y: 488 },
      { x: 631, y: 173 },
      { x: 631, y: 340 },
      { x: 633, y: 476 },
      { x: 634, y: 627 },
      { x: 635, y: 430 },
      { x: 635, y: 477 },
      { x: 636, y: 133 },
      { x: 637, y: 502 },
      { x: 639, y: 551 },
      { x: 64, y: 178 },
      { x: 64, y: 524 },
      { x: 641, y: 77 },
      { x: 645, y: 239 },
      { x: 646, y: 329 },
      { x: 648, y: 110 },
      { x: 65, y: 487 },
      { x: 650, y: 499 },
      { x: 652, y: 193 },
      { x: 654, y: 21 },
      { x: 655, y: 593 },
      { x: 657, y: 570 },
      { x: 659, y: 222 },
      { x: 659, y: 326 },
      { x: 66, y: 376 },
      { x: 663, y: 411 },
      { x: 663, y: 617 },
      { x: 664, y: 41 },
      { x: 665, y: 320 },
      { x: 667, y: 368 },
      { x: 667, y: 431 },
      { x: 668, y: 101 },
      { x: 668, y: 375 },
      { x: 671, y: 107 },
      { x: 671, y: 317 },
      { x: 672, y: 416 },
      { x: 678, y: 432 },
      { x: 679, y: 125 },
      { x: 679, y: 514 },
      { x: 680, y: 66 },
      { x: 681, y: 151 },
      { x: 684, y: 175 },
      { x: 685, y: 386 },
      { x: 686, y: 271 },
      { x: 686, y: 434 },
      { x: 686, y: 482 },
      { x: 686, y: 611 },
      { x: 687, y: 183 },
      { x: 689, y: 553 },
      { x: 69, y: 49 },
      { x: 693, y: 391 },
      { x: 695, y: 527 },
      { x: 696, y: 205 },
      { x: 698, y: 534 },
      { x: 699, y: 148 },
      { x: 7, y: 72 },
      { x: 70, y: 138 },
      { x: 703, y: 347 },
      { x: 704, y: 273 },
      { x: 704, y: 641 },
      { x: 705, y: 280 },
      { x: 706, y: 384 },
      { x: 706, y: 636 },
      { x: 709, y: 564 },
      { x: 711, y: 480 },
      { x: 715, y: 422 },
      { x: 717, y: 250 },
      { x: 717, y: 505 },
      { x: 717, y: 82 },
      { x: 719, y: 115 },
      { x: 719, y: 146 },
      { x: 719, y: 638 },
      { x: 72, y: 346 },
      { x: 724, y: 264 },
      { x: 727, y: 410 },
      { x: 729, y: 185 },
      { x: 732, y: 160 },
      { x: 732, y: 227 },
      { x: 734, y: 267 },
      { x: 737, y: 118 },
      { x: 738, y: 1 },
      { x: 739, y: 195 },
      { x: 739, y: 85 },
      { x: 745, y: 244 },
      { x: 747, y: 304 },
      { x: 748, y: 105 },
      { x: 750, y: 278 },
      { x: 750, y: 309 },
      { x: 750, y: 403 },
      { x: 752, y: 178 },
      { x: 752, y: 422 },
      { x: 755, y: 192 },
      { x: 759, y: 217 },
      { x: 759, y: 575 },
      { x: 760, y: 85 },
      { x: 761, y: 27 },
      { x: 762, y: 294 },
      { x: 762, y: 406 },
      { x: 766, y: 185 },
      { x: 767, y: 635 },
      { x: 768, y: 531 },
      { x: 77, y: 118 },
      { x: 77, y: 297 },
      { x: 77, y: 528 },
      { x: 77, y: 575 },
      { x: 771, y: 572 },
      { x: 772, y: 55 },
      { x: 773, y: 95 },
      { x: 774, y: 470 },
      { x: 776, y: 446 },
      { x: 777, y: 104 },
      { x: 779, y: 653 },
      { x: 780, y: 272 },
      { x: 780, y: 560 },
      { x: 781, y: 516 },
      { x: 783, y: 424 },
      { x: 784, y: 204 },
      { x: 785, y: 1 },
      { x: 787, y: 19 },
      { x: 789, y: 353 },
      { x: 789, y: 506 },
      { x: 790, y: 236 },
      { x: 790, y: 417 },
      { x: 791, y: 184 },
      { x: 792, y: 81 },
      { x: 794, y: 129 },
      { x: 796, y: 315 },
      { x: 796, y: 38 },
      { x: 797, y: 157 },
      { x: 797, y: 278 },
      { x: 797, y: 303 },
      { x: 797, y: 50 },
      { x: 8, y: 550 },
      { x: 800, y: 170 },
      { x: 801, y: 339 },
      { x: 802, y: 229 },
      { x: 803, y: 260 },
      { x: 805, y: 285 },
      { x: 805, y: 5 },
      { x: 805, y: 546 },
      { x: 806, y: 313 },
      { x: 806, y: 479 },
      { x: 809, y: 381 },
      { x: 810, y: 491 },
      { x: 810, y: 74 },
      { x: 812, y: 359 },
      { x: 816, y: 327 },
      { x: 818, y: 215 },
      { x: 818, y: 582 },
      { x: 818, y: 584 },
      { x: 820, y: 193 },
      { x: 822, y: 390 },
      { x: 826, y: 101 },
      { x: 828, y: 359 },
      { x: 831, y: 257 },
      { x: 831, y: 434 },
      { x: 832, y: 406 },
      { x: 842, y: 584 },
      { x: 843, y: 449 },
      { x: 843, y: 625 },
      { x: 847, y: 170 },
      { x: 85, y: 6 },
      { x: 854, y: 512 },
      { x: 856, y: 88 },
      { x: 857, y: 25 },
      { x: 859, y: 146 },
      { x: 861, y: 28 },
      { x: 861, y: 291 },
      { x: 861, y: 452 },
      { x: 862, y: 400 },
      { x: 863, y: 305 },
      { x: 863, y: 466 },
      { x: 863, y: 498 },
      { x: 867, y: 472 },
      { x: 867, y: 631 },
      { x: 869, y: 653 },
      { x: 87, y: 37 },
      { x: 875, y: 577 },
      { x: 876, y: 275 },
      { x: 879, y: 409 },
      { x: 88, y: 216 },
      { x: 88, y: 327 },
      { x: 90, y: 370 },
      { x: 90, y: 515 },
      { x: 93, y: 192 },
      { x: 95, y: 584 },
      { x: 96, y: 377 },
    ],
  },
  ranges: {
    x: { min: 0, max: 900 },
    y: { min: 0, max: 700 },
  },
}
