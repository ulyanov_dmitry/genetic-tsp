import styled from 'styled-components'

const Heading = styled.h1`
  font-size: 34px;
  font-weight: 900;
  margin-top: 0;
`

export default Heading
