import React from 'react'
import { connect } from 'react-redux'

import { AppScreen, AppScreenWrapper, Button, Container, Top } from './styled'
import { Canvas, Heading } from 'components'

import { convertRange } from 'utils/convertRange'
import { POINTS } from 'data/points'
import { runAlgo, stopAlgo } from './heap'

const convertX = convertRange(POINTS.ranges.x)
const convertY = convertRange(POINTS.ranges.y)

class App extends React.Component {
  containerRef = React.createRef()

  state = {
    measurements: {
      width: null,
      height: null,
    },
    points: null,
    populationSize: 30,
  }

  componentDidMount() {
    this.updateMeasurement()
  }

  setInitialPoints = () => {
    const { measurements } = this.state

    const points = POINTS.byNumber['40'].map(({ x, y }) => {
      return {
        x: convertX({ min: 10, max: measurements.width }, x),
        y: convertY({ min: 10, max: measurements.height }, y),
      }
    })

    this.setState({ points })
  }

  updateMeasurement = () => {
    const container = this.containerRef.current
    if (container) {
      this.setState(
        {
          measurements: {
            height: container.offsetHeight,
            width: container.offsetWidth,
          },
        },
        this.setInitialPoints,
      )
    }
  }

  run = () => {
    runAlgo(this.state.points)
  }

  stop = () => {
    stopAlgo()
  }

  render() {
    const { measurements, points } = this.state
    const { best, isRunning } = this.props

    return (
      <AppScreenWrapper>
        <AppScreen>
          <Top>
            <Heading>TSP Genetic Algorithm</Heading>
            <Button
              disabled={!points}
              onClick={isRunning ? this.stop : this.run}>
              {isRunning ? 'Stop algorithm' : 'Run algorithm'}
            </Button>
          </Top>
          <Container innerRef={this.containerRef}>
            {points && (
              <Canvas best={best} measurements={measurements} points={points} />
            )}
          </Container>
        </AppScreen>
      </AppScreenWrapper>
    )
  }
}

export default connect(state => ({
  best: state.best,
  isRunning: state.running,
}))(App)
