import styled from 'styled-components'
import { createBoxShadow } from 'styles'

import ButtonMaterial from '@material-ui/core/Button'

export const AppScreen = styled.div`
  background: #202124;
  border-radius: 8px;
  box-shadow: ${createBoxShadow()};
  display: flex;
  flex-direction: column;
  flex: 1;
  padding: 32px;
`

export const AppScreenWrapper = styled.div`
  align-items: stretch;
  background: linear-gradient(45deg, #355c7d, #6c5b7b, #c06c84);
  display: flex;
  flex: 1;
  font-family: Roboto, sans-serif;
  justify-content: center;
  padding: 16px;
`

export const Button = styled(ButtonMaterial).attrs({ variant: 'contained' })`
  align-self: flex-start;
  background: darkviolet;
`

export const Container = styled.div`
  width: 100%;
  height: 100%;
`

export const Top = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`
