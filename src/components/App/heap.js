import { dispatch } from 'store'
import { updateState } from 'store/app'

let ctx
let WIDTH, HEIGHT
let points = []
export let running

let POPULATION_SIZE
let CROSSOVER_PROBABILITY
let MUTATION_PROBABILITY

let mutationTimes
let dis
let bestValue, best
let currentGeneration
let currentBest
let population
let values
let fitnessValues
let roulette

const $ = () => $

export function runAlgo(pointsInput) {
  init()
  initData()
  points = pointsInput
  if (points.length >= 3) {
    initData()
    GAInitialize()
    running = true
    dispatch(updateState({ running: true }))
  } else {
    alert('add some more points to the map!')
  }
}

function init() {
  setInterval(draw, 10)
}

function initData() {
  running = false
  POPULATION_SIZE = 30
  CROSSOVER_PROBABILITY = 0.9
  MUTATION_PROBABILITY = 0.01
  mutationTimes = 0

  bestValue = undefined
  best = []
  currentGeneration = 0
  population = []
  values = new Array(POPULATION_SIZE)
  fitnessValues = new Array(POPULATION_SIZE)
  roulette = new Array(POPULATION_SIZE)
}

function drawCircle(point) {
  ctx.fillStyle = '#000'
  ctx.beginPath()
  ctx.arc(point.x, point.y, 3, 0, Math.PI * 2, true)
  ctx.closePath()
  ctx.fill()
}
function drawLines(array) {
  ctx.strokeStyle = '#f00'
  ctx.lineWidth = 1
  ctx.beginPath()

  ctx.moveTo(points[array[0]].x, points[array[0]].y)
  for (let i = 1; i < array.length; i++) {
    ctx.lineTo(points[array[i]].x, points[array[i]].y)
  }
  ctx.lineTo(points[array[0]].x, points[array[0]].y)

  ctx.stroke()
  ctx.closePath()
}
function draw() {
  if (running) {
    GANextGeneration()
    if (best.length === points.length) {
      dispatch(updateState({ best }))
    }
    //$('#status').text(
    //  'There are ' +
    //    points.length +
    //    ' cities in the map, ' +
    //    'the ' +
    //    currentGeneration +
    //    'th generation with ' +
    //    mutationTimes +
    //    ' times of mutation. best value: ' +
    //    ~~bestValue,
    //)
  } else {
    //$('#status').text('There are ' + points.length + ' Cities in the map. ')
  }
}

function GAInitialize() {
  countDistances()
  for (let i = 0; i < POPULATION_SIZE; i++) {
    population.push(randomIndivial(points.length))
  }
  setBestValue()
}
function GANextGeneration() {
  currentGeneration++
  selection()
  crossover()
  mutation()

  setBestValue()
}

function selection() {
  let parents = []
  let initnum = 4
  parents.push(population[currentBest.bestPosition])
  parents.push(doMutate(clone(best)))
  parents.push(pushMutate(clone(best)))
  parents.push(clone(best))

  setRoulette()
  for (let i = initnum; i < POPULATION_SIZE; i++) {
    parents.push(population[wheelOut(Math.random())])
  }
  population = parents
}
function crossover() {
  let queue = []
  for (let i = 0; i < POPULATION_SIZE; i++) {
    if (Math.random() < CROSSOVER_PROBABILITY) {
      queue.push(i)
    }
  }
  shuffle(queue)
  for (let i = 0, j = queue.length - 1; i < j; i += 2) {
    doCrossover(queue[i], queue[i + 1])
    //oxCrossover(queue[i], queue[i+1]);
  }
}
//function oxCrossover(x, y) {
//  //let px = population[x].roll();
//  //let py = population[y].roll();
//  let px = population[x].slice(0);
//  let py = population[y].slice(0);

//  let rand = randomNumber(points.length-1) + 1;
//  let pre_x = px.slice(0, rand);
//  let pre_y = py.slice(0, rand);

//  let tail_x = px.slice(rand, px.length);
//  let tail_y = py.slice(rand, py.length);

//  px = tail_x.concat(pre_x);
//  py = tail_y.concat(pre_y);

//  population[x] = pre_y.concat(px.reject(pre_y));
//  population[y] = pre_x.concat(py.reject(pre_x));
//}
function doCrossover(x, y) {
  let child1 = getChild(next, x, y)
  let child2 = getChild(previous, x, y)
  population[x] = child1
  population[y] = child2
}
function getChild(fun, x, y) {
  let solution = []
  let px = clone(population[x])
  let py = clone(population[y])
  let dx, dy
  let c = px[randomNumber(px.length)]
  solution.push(c)
  while (px.length > 1) {
    dx = fun(px, px.indexOf(c))
    dy = fun(py, py.indexOf(c))
    deleteByValue(px, c)
    deleteByValue(py, c)
    c = dis[c][dx] < dis[c][dy] ? dx : dy
    solution.push(c)
  }
  return solution
}
function mutation() {
  for (let i = 0; i < POPULATION_SIZE; i++) {
    if (Math.random() < MUTATION_PROBABILITY) {
      if (Math.random() > 0.5) {
        population[i] = pushMutate(population[i])
      } else {
        population[i] = doMutate(population[i])
      }
      i--
    }
  }
}

function doMutate(seq) {
  mutationTimes++
  // m and n refers to the actual index in the array
  // m range from 0 to length-2, n range from 2...length-m
  let m, n
  do {
    m = randomNumber(seq.length - 2)
    n = randomNumber(seq.length)
  } while (m >= n)

  for (let i = 0, j = (n - m + 1) >> 1; i < j; i++) {
    swap(seq, m + i, n - i)
  }
  return seq
}
function pushMutate(seq) {
  mutationTimes++
  let m, n
  do {
    m = randomNumber(seq.length >> 1)
    n = randomNumber(seq.length)
  } while (m >= n)

  let s1 = seq.slice(0, m)
  let s2 = seq.slice(m, n)
  let s3 = seq.slice(n, seq.length)
  return clone(s2.concat(s1).concat(s3))
}
function setBestValue() {
  for (let i = 0; i < population.length; i++) {
    values[i] = evaluate(population[i])
  }
  currentBest = getCurrentBest()
  if (bestValue === undefined || bestValue > currentBest.bestValue) {
    best = clone(population[currentBest.bestPosition])
    bestValue = currentBest.bestValue
  } else {
  }
}
function getCurrentBest() {
  let bestP = 0,
    currentBestValue = values[0]

  for (let i = 1; i < population.length; i++) {
    if (values[i] < currentBestValue) {
      currentBestValue = values[i]
      bestP = i
    }
  }
  return {
    bestPosition: bestP,
    bestValue: currentBestValue,
  }
}
function setRoulette() {
  //calculate all the fitness
  for (let i = 0; i < values.length; i++) {
    fitnessValues[i] = 1.0 / values[i]
  }
  //set the roulette
  let sum = 0
  for (let i = 0; i < fitnessValues.length; i++) {
    sum += fitnessValues[i]
  }
  for (let i = 0; i < roulette.length; i++) {
    roulette[i] = fitnessValues[i] / sum
  }
  for (let i = 1; i < roulette.length; i++) {
    roulette[i] += roulette[i - 1]
  }
}
function wheelOut(rand) {
  let i
  for (i = 0; i < roulette.length; i++) {
    if (rand <= roulette[i]) {
      return i
    }
  }
}
function randomIndivial(n) {
  let a = []
  for (let i = 0; i < n; i++) {
    a.push(i)
  }
  return shuffle(a)
}
function evaluate(indivial) {
  let sum = dis[indivial[0]][indivial[indivial.length - 1]]
  for (let i = 1; i < indivial.length; i++) {
    sum += dis[indivial[i]][indivial[i - 1]]
  }
  return sum
}
function countDistances() {
  let length = points.length
  dis = new Array(length)
  for (let i = 0; i < length; i++) {
    dis[i] = new Array(length)
    for (let j = 0; j < length; j++) {
      dis[i][j] = ~~distance(points[i], points[j])
    }
  }
}

function clone(arr) {
  return [...arr]
}

function shuffle(arr) {
  for (
    let j, x, i = arr.length - 1;
    i;
    j = randomNumber(i), x = arr[--i], arr[i] = arr[j], arr[j] = x
  );
  return arr
}

function deleteByValue(arr, value) {
  let pos = arr.indexOf(value)
  arr.splice(pos, 1)
}
function swap(arr, x, y) {
  if (x > arr.length || y > arr.length || x === y) {
    return
  }
  let tem = arr[x]
  arr[x] = arr[y]
  arr[y] = tem
}

function randomNumber(boundary) {
  return parseInt(Math.random() * boundary, 10)
}
function distance(p1, p2) {
  return euclidean(p1.x - p2.x, p1.y - p2.y)
}
function euclidean(dx, dy) {
  return Math.sqrt(dx * dx + dy * dy)
}
function next(arr, index) {
  if (index === arr.length - 1) {
    return arr[0]
  } else {
    return arr[index + 1]
  }
}
function previous(arr, index) {
  if (index === 0) {
    return arr[arr.length - 1]
  } else {
    return arr[index - 1]
  }
}

export function stopAlgo() {
  running = false
  dispatch(updateState({ running: false }))
}
