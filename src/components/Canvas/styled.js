import styled from 'styled-components'

import { Stage } from 'react-konva'
import { createBoxShadow } from 'styles'

export const StyledStage = styled(Stage)`
  background: #434449;
  border-radius: 4px;
  box-shadow: ${createBoxShadow()};
`
