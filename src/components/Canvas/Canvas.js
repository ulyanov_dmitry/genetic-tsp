import React from 'react'
import PropTypes from 'prop-types'

import { StyledStage } from './styled'
import { Circle, Layer, Line } from 'react-konva'

class Canvas extends React.Component {
  renderAllLines = () => {
    return this.props.points.map(({ x, y }, index) => {
      const from = this.props.points[0]
      return (
        <Line
          stroke="palevioletred"
          key={`line-x${x}-y${y}`}
          points={[from.x, from.y, x, y]}
        />
      )
    })
  }

  renderBest = () => {
    const { best, points } = this.props
    const lines = []

    for (let i = 0; i < best.length - 1; i++) {
      const from = points[best[i]]
      const to = points[best[i + 1]]
      lines.push(
        <Line
          stroke="palevioletred"
          key={`line-${from.x}&${from.y}-${to.x}&${to.y}`}
          points={[from.x, from.y, to.x, to.y]}
          strokeWidth={1.44}
        />,
      )
    }

    return lines
  }

  render() {
    const { best, measurements, points } = this.props

    return (
      <StyledStage width={measurements.width} height={measurements.height}>
        <Layer>
          {best && this.renderBest()}
          {points.map(({ x, y }) => {
            return (
              <Circle
                fill="turquoise"
                key={`x${x}-y${y}`}
                radius={4}
                x={x}
                y={y}
              />
            )
          })}
        </Layer>
      </StyledStage>
    )
  }
}

Canvas.propTypes = {
  best: PropTypes.array,
  measurements: PropTypes.object,
  points: PropTypes.array,
}

export default Canvas
